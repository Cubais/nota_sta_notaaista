-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "MoveUnit",
		parameterDefs = {
			{ 
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
            { 
				name = "point",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

function EnemiesInArea(point, radius)
    local enemyIDs = Sensors.core.EnemyTeamIDs()
    for _, enemyID in ipairs(enemyIDs) do    
        if (Spring.GetUnitsInCylinder(point.x, point.z, radius, enemyID)[1] ~= nil) then
            return true
        end
    end
    
    return false
end


function Run(self, units, parameter)

    if not Spring.ValidUnitID(parameter.unitID) then
		return FAILURE
	end			
	
	if not self.initiaized then 
        local x, y, z = Spring.GetUnitPosition(parameter.unitID)
        if ((Vec3(x, y, z) - parameter.point):Length() < 1500) then
            Spring.GiveOrderToUnit(parameter.unitID, CMD.FIRE_STATE, {0}, {})
            Spring.GiveOrderToUnit(parameter.unitID, CMD.ATTACK, {parameter.point.x, parameter.point.y, parameter.point.z, 150}, {"shift"})        
        end
		self.initiaized = true  
        return SUCCESS
	end
    
	return RUNNING
end


function Reset(self)
	self.initiaized = false
end
