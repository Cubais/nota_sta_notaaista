-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "MoveUnit",
		parameterDefs = {
			{ 
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
            { 
				name = "destination",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
            { 
				name = "safePoint",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}            
		}
	}
end

function EnemiesInArea(point, radius)
    local enemyIDs = Sensors.core.EnemyTeamIDs()
    for _, enemyID in ipairs(enemyIDs) do    
        if (Spring.GetUnitsInCylinder(point.x, point.z, radius, enemyID)[1] ~= nil) then
            return true
        end
    end
    
    return false
end

local function ClearState(self)
	self.initiaized = false
end

function Run(self, units, parameter)

    if not Spring.ValidUnitID(parameter.unitID) then
		return FAILURE
	end		
	
	if not self.initiaized then    
        Spring.GiveOrderToUnit(parameter.unitID, CMD.MOVE, {parameter.destination.x, parameter.destination.y, parameter.destination.z}, {})
		Spring.GiveOrderToUnit(parameter.unitID, CMD.RECLAIM, {parameter.destination.x, parameter.destination.y, parameter.destination.z, 400}, {"shift"})        
		self.initiaized = true        
	else    
        local x, y , z = Spring.GetUnitPosition(parameter.unitID)
        local enemiesAroundFarck = EnemiesInArea(Vec3(x, y, z), 500)
        
        if (self.initiaized and enemiesAroundFarck) then
            Spring.GiveOrderToUnit(parameter.unitID, CMD.MOVE, {parameter.safePoint.x, parameter.safePoint.y, parameter.safePoint.z}, {})
        elseif (self.initiaized and not enemiesAroundFarck and (Spring.GetUnitCommands(parameter.unitID, 5)[1] == nil) or (Vec3(x, y, z) - parameter.safePoint):Length() < 200) then
            Spring.GiveOrderToUnit(parameter.unitID, CMD.RECLAIM, {parameter.destination.x, parameter.destination.y, parameter.destination.z, 600}, {})        
        end
    end
	
	return RUNNING
end

function Reset(self)
	ClearState(self)
end
