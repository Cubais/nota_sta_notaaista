function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Moves along safe path with least enemy possible",
		parameterDefs = {
			{ 
				name = "transporterID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "destination",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "costTable", -- relative formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
--local SafePath = FindSafePath


local function ClearState(self)
	self.initiaized = false
    self.destination = nil
end

function Split(s, delimiter)
    result = {};
    for match in (s..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(result, match);
    end
    return result;
end

function GetMapGridPositionVec3(position)
	local rectSizeLength = Game.mapSizeX / 64	
	
	-- determine x and z position of square we are into
	local rectX = math.floor(position.x / rectSizeLength)
	local rectZ = math.floor(position.z / rectSizeLength)
	
	-- return center position of the square
	return Vec3(rectX, y, rectZ)
end

function Run(self, units, parameter)
	
	if not Spring.ValidUnitID(parameter.transporterID) then
		return FAILURE
	end		
	
	local x, y, z = Spring.GetUnitPosition(parameter.transporterID)
	local nearDestination = self.destination ~= nil and (math.abs(x - self.destination.x) < 300 and math.abs(z - self.destination.z) < 300)
	-- reached end of the path
	if (self.initiaized and nearDestination) then
		return SUCCESS
	end
	
	if not self.initiaized then
    
		-- get path nodes
		local pathToFollow = Sensors.FindSafePath(parameter.transporterID, parameter.destination, parameter.costTable)
        local count = 0
        local lastMovePosition = nil
		for key, position in ipairs(pathToFollow) do		
            count = count + 1
            lastMovePosition = key
			SpringGiveOrderToUnit(parameter.transporterID, CMD.MOVE, {position.x, position.y, position.z}, {"shift"})
		end
        
		self.initiaized = true
        self.destination = pathToFollow[lastMovePosition]
        if (count < 5) then
            return FAILURE
        end
	end
	
	return RUNNING
end


function Reset(self)
	ClearState(self)
end
