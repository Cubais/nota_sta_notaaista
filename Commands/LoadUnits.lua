function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Load given unit",
		parameterDefs = {
			{ 
				name = "transporterID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unitToTransportID", -- relative formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.initiaized = false
end

function Run(self, units, parameter)
	
	if not Spring.ValidUnitID(parameter.transporterID) then
		return FAILURE
	end
	
	if not Spring.ValidUnitID(parameter.unitToTransportID) then
		return FAILURE
	end
	
	if Spring.GetUnitTransporter(parameter.unitToTransportID) ~= nil then
		return SUCCESS
	end
	
	if not self.initiaized then
		SpringGiveOrderToUnit(parameter.transporterID, CMD.LOAD_UNITS, {parameter.unitToTransportID}, {})
		self.initiaized = true
	end
    
    if (self.initiaized and Spring.GetUnitCommands(parameter.transporterID, 5)[1] == nil) then
        SpringGiveOrderToUnit(parameter.transporterID, CMD.LOAD_UNITS, {parameter.unitToTransportID}, {})
    end
	
	return RUNNING
end


function Reset(self)
	ClearState(self)
end
