-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "MoveUnit",
		parameterDefs = {
			{ 
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
            { 
				name = "destination",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

local function ClearState(self)
	self.initiaized = false
end

function Run(self, units, parameter)

    if not Spring.ValidUnitID(parameter.unitID) then
		return FAILURE
	end		
	
	local x, y, z = Spring.GetUnitPosition(parameter.unitID)
	local nearDestination = (math.abs(x - parameter.destination.x) < 300 and math.abs(z - parameter.destination.z) < 300)
	-- reached end of the path
	if (self.initiaized and nearDestination) then
		return SUCCESS
	end
	
	if not self.initiaized then    
		Spring.GiveOrderToUnit(parameter.unitID, CMD.MOVE, {parameter.destination.x, parameter.destination.y, parameter.destination.z}, {})        
		self.initiaized = true        
	end
    
    if (self.initiaized and Spring.GetUnitCommands(parameter.unitID, 5)[1] == nil) then
        Spring.GiveOrderToUnit(parameter.unitID, CMD.MOVE, {parameter.destination.x, parameter.destination.y, parameter.destination.z}, {})
    end
	
	return RUNNING
end

function Reset(self)
	ClearState(self)
end
