function getInfo()
	return {
		onNoUnits = RUNNING, -- instant success
		tooltip = "Scouts map",
		parameterDefs = {
			{ 
				name = "scoutingUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
--local SafePath = FindSafePath


local function ClearState(self)
	self.initialized = false
end

function CommandQueueEmpty(unitsList)

	--[[local nonNilUnits = {}
	local count = 1
	for _, id in pairs(unitsList) do 
		if (id ~= nil) then
			nonNilUnits[count] = id
			count = count + 1
		end
	end]]--
	
	local empty = true
	for _, id in ipairs(unitsList) do 		
		if (Spring.ValidUnitID(id) and Spring.GetUnitCommands(id, 5)[1] ~= nil) then			
			empty = false
			break
		end
	end
	
	return empty
end

function Run(self, units, parameter)
	
    if #parameter.scoutingUnits == 0 then
		return FAILURE
	end		
		
	if self.initialized and CommandQueueEmpty(parameter.scoutingUnits) then
		return SUCCESS
	end
	
	if not self.initialized then		
		local spacingX = Game.mapSizeX / #parameter.scoutingUnits
		local mapSizeZ = Game.mapSizeZ
				
		for index, unitID in ipairs(parameter.scoutingUnits) do		
            
			local x, z = index * spacingX - (spacingX / 2), 250	
			local y = Spring.GetGroundHeight(x, z)
			-- Beginning of the map
			SpringGiveOrderToUnit(unitID, CMD.MOVE, {x, y, z}, {"shift"})
			
			z = mapSizeZ - 250
			y = Spring.GetGroundHeight(x, z)
			
			-- end of the map
			SpringGiveOrderToUnit(unitID, CMD.MOVE, {x, y, z}, {"shift"})
		end
		self.initialized = true
	end
	
	return RUNNING
end


function Reset(self)
	ClearState(self)
end
