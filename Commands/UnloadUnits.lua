function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Unload unit at given location",
		parameterDefs = {
			{ 
				name = "transporterID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unitToTransportID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "radius",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
            { 
				name = "preventMovement",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.initiaized = false
end

function Run(self, units, parameter)
	
	if not Spring.ValidUnitID(parameter.transporterID) then
		return FAILURE
	end
	
	if not Spring.ValidUnitID(parameter.unitToTransportID) then
		return FAILURE
	end
		
	if Spring.GetUnitTransporter(parameter.unitToTransportID) == nil then
        local x,y,z = SpringGetUnitPosition(parameter.unitToTransportID)
        if (parameter.preventMovement == true) then
            SpringGiveOrderToUnit(parameter.unitToTransportID, CMD.MOVE, {parameter.position.x, parameter.position.y, parameter.position.z}, {"shift"})
        end
		return SUCCESS
	end
	
	if not self.initiaized then
		SpringGiveOrderToUnit(parameter.transporterID, CMD.UNLOAD_UNITS, {parameter.position.x, parameter.position.y, parameter.position.z, parameter.radius}, {})
		self.initiaized = true
	end
    
    if (self.initiaized and Spring.GetUnitCommands(parameter.transporterID, 5)[1] == nil) then        
        SpringGiveOrderToUnit(parameter.transporterID, CMD.MOVE, {parameter.position.x, parameter.position.y, parameter.position.z}, {})
        SpringGiveOrderToUnit(parameter.transporterID, CMD.UNLOAD_UNITS, {parameter.position.x + 50, parameter.position.y, parameter.position.z, 200}, {"shift"})
    end
	
	return RUNNING
end


function Reset(self)
	ClearState(self)
end
