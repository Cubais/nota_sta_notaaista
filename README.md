nota_sta_Labs
====

- [dependencies](./dependencies.json)

Trees
----

* sandsailTree
* ctp2
* ttdr
* ttdr-SaveOneUnit
* UnitTypeTest
* Pathfinding

Sensors
----

* ConcatTables
* dotProduct
* CreateReservationSystem (ttdr)
* DetectHills
* EnemiesGridCosts (ttdr)
* FindSafePath (implemented simplified A*)
* FreeUnits (ttdr)
* GetFreeTransporter (ttdr)
* GetUnitPosition
* GetUnitToRescue (ttdr)
* HighlightDangerCells (debug)
* HighlightEnemyUnitsCell (debug)
* InRectangleDebug
* MaxWeaponRange
* SafeRescueUnits (ttdr)

Commands
----

* UnloadUnits (ttdr)
* LoadUnits (ttdr)
* FollowSafePath (ttdr)
* ScoutArea (ttdr)


