local sensorInfo = {
	name = "UnitTypeTest",
	desc = "Returns type of the unit",
	author = "Jakub Stacho",
	date = "2021-07-01",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

function NumEntries(lineTable)
    local count = 0
    for _,_ in pairs(lineTable) do
        count = count + 1
    end
    
    return count
end

-- @description return static position of the first unit
return function(mavericsTable, category)
    
    if (mavericsTable == nil) then
        mavericsTable = {}
    end
    
    for id,_ in pairs(mavericsTable) do
        if (not Spring.ValidUnitID(id)) then
            mavericsTable[id] = nil
        end
    end 
    
    local units = Spring.GetUnitsInCylinder( 1400, 8900, 1000)
    local maverics = Sensors.core.FilterUnitsByCategory(units, category)
    for _, id in ipairs(maverics) do
        mavericsTable[id] = 0
    end
    
    return { num = NumEntries(mavericsTable), units = mavericsTable}
end