local sensorInfo = {
	name = "UnitTypeTest",
	desc = "Returns type of the unit",
	author = "Jakub Stacho",
	date = "2021-07-01",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local armmartCount = global.group.armmart
local armboxCount = global.group.armbox
local armatlasCount = global.group.armatlas
local armmavCount = global.group.armmav
local armseerCount = global.group.armseer

function HasSpace(unitName, unitsTable) 
    
    local count = 0
    for _, unit in pairs(unitsTable[unitName]) do
        count = count + 1
    end
    
    return (count < global.group[unitName])    
end

-- @description return static position of the first unit
return function(supportUnits, reservationUnits)

    if (supportUnits == nil) then
        supportUnits = {}
        supportUnits.armbox = {}
        supportUnits.armmart = {}
        supportUnits.armmav = {}
        supportUnits.armatlas = {}
        supportUnits.armseer = {}
    end
    
    local newUnits = Spring.GetUnitsInCylinder( 1400, 8900, 1000)
    for _, unit in ipairs(newUnits) do
        local unitType = UnitDefs[Spring.GetUnitDefID(unit)].name         
        if (supportUnits[unitType] ~= nil and supportUnits[unitType][unit] == nil and reservationUnits[unit] == nil and HasSpace(unitType, supportUnits)) then
            supportUnits[unitType][unit] = "free"
            reservationUnits[unit] = "taken"
        end    
    end
    
    return supportUnits
end