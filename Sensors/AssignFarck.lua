local sensorInfo = {
	name = "UnitTypeTest",
	desc = "Returns type of the unit",
	author = "Jakub Stacho",
	date = "2021-07-01",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

function GetFreeFarck(unitFarcks, farckUnitsTable)
    local occupied = false    
    for _, farck in pairs(unitFarcks) do        
        for k, v in pairs(farckUnitsTable) do
            for num, id in pairs(v) do
                if (id == farck) then
                    occupied = true
                    break    
                end
                
                if (occupied == true) then 
                    break
                end
            end            
        end
        
        if (occupied == false) then
            return farck
        end  
        occupied = false
    end
    
    return nil
end

function NumEntries(lineTable)
    local count = 0
    for _,_ in pairs(lineTable) do
        count = count + 1
    end
    
    return count
end

-- @description return static position of the first unit
return function(farckUnits, category)

    if (farckUnits == nil) then
        farckUnits = {}
        farckUnits.Middle = {}
        farckUnits.Top = {}
        farckUnits.Bottom = {}
    end

    local units = Spring.GetUnitsInCylinder( 1400, 8900, 1000)
    local farcks = Sensors.core.FilterUnitsByCategory(units, category)
    
    -- check for dead farcks
    for k,v in pairs(farckUnits) do
        for num, id in pairs(v) do
            if (not Spring.ValidUnitID(id)) then
                farckUnits[k][num] = nil
            end
        end
        
    end    
    
    if (NumEntries(farckUnits.Middle) < 1) then 
        local freeFarckID = GetFreeFarck(farcks, farckUnits)
        if (freeFarckID ~= nil) then
            if (farckUnits.Middle[1] == nil) then
                farckUnits.Middle[1] = freeFarckID
            else
                farckUnits.Middle[2] = freeFarckID
            end
        end
    end
    
   if (NumEntries(farckUnits.Top) < 1) then        
        local freeFarckID = GetFreeFarck(farcks, farckUnits)
        if (freeFarckID ~= nil) then
            if (farckUnits.Top[1] == nil) then
                farckUnits.Top[1] = freeFarckID
            else
                farckUnits.Top[2] = freeFarckID
            end
        end
    end
    
    if (NumEntries(farckUnits.Bottom) < 1) then        
        local freeFarckID = GetFreeFarck(farcks, farckUnits)
        if (freeFarckID ~= nil) then
            if (farckUnits.Bottom[1] == nil) then
                farckUnits.Bottom[1] = freeFarckID
            else
                farckUnits.Bottom[2] = freeFarckID
            end
        end
    end
    
    return farckUnits
end