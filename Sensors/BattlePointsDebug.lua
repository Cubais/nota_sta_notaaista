local sensorInfo = {
	name = "InRectangleDebug",
	desc = "Highlights rectangle on map based on unit position",
	author = "Cubais",
	date = "2021-07-03",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description returns unit position and X and Z coordinate of rectangle
return function(battlePoints)
	
    local key = 1
    for _, value in pairs(battlePoints) do     
        if (Script.LuaUI('exampleDebug_update')) then
            Script.LuaUI.exampleDebug_update(
                key, -- key
                {	-- data
                    startPos = value.own, 
                    endPos = value.own + Vec3(100, 0, 0)
                }
            )            
        end
        key = key + 1
        
        if (Script.LuaUI('exampleDebug_update')) then
            Script.LuaUI.exampleDebug_update(
                key, -- key
                {	-- data
                    startPos = value.enemy, 
                    endPos = value.enemy + Vec3(100, 0, 0)
                }
            )            
        end
        key = key + 1
        
        if (Script.LuaUI('exampleDebug_update')) then
            Script.LuaUI.exampleDebug_update(
                key, -- key
                {	-- data
                    startPos = value.safe, 
                    endPos = value.safe + Vec3(100, 0, 0)
                }
            )            
        end
        key = key + 1
    end
		
	return true
end