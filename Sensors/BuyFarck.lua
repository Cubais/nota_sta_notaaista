local sensorInfo = {
	name = "UnitTypeTest",
	desc = "Returns type of the unit",
	author = "Jakub Stacho",
	date = "2021-07-01",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

function BuyUnit(unit)
    global.price = global.price + 200
    message.SendRules({
        subject = "swampdota_buyUnit",
        data = {
            unitName = unit
        },
    })
    
end

function NumEntries(lineTable)
    local count = 0
    for _,_ in pairs(lineTable) do
        count = count + 1
    end
    
    return count
end

-- @description return static position of the first unit
return function(farckUnits)

    if (farckUnits == nil) then
        farckUnits = {}
        farckUnits.Middle = {}
        farckUnits.Top = {}
        farckUnits.Bottom = {}
    end
    
    local info = Sensors.core.MissionInfo()
    local farckCost = info.buy["armfark"]
    local teamMetal = Sensors.core.TeamMetal()
    teamMetal.currentLevel = teamMetal.currentLevel - global.price
    
    if (NumEntries(farckUnits.Middle) < 1 and farckCost < teamMetal.currentLevel) then        
        BuyUnit("armfark")        
        return farckUnits 
    end
    
    if (NumEntries(farckUnits.Top) < 1 and farckCost < teamMetal.currentLevel) then        
        BuyUnit("armfark")        
        return farckUnits 
    end
    
    if (NumEntries(farckUnits.Bottom) < 1 and farckCost < teamMetal.currentLevel) then        
        BuyUnit("armfark")        
        return farckUnits 
    end
    
    return farckUnits
end