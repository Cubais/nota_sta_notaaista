local sensorInfo = {
	name = "UnitTypeTest",
	desc = "Returns type of the unit",
	author = "Jakub Stacho",
	date = "2021-07-01",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return static position of the first unit
return function(missionInfo, unit)
    local teamMetal = Sensors.core.TeamMetal()    
    local unitCost = missionInfo.buy[unit]
    
    if (unitCost * 2 + global.price < teamMetal.currentLevel) then
        global.price = global.price + unitCost
        message.SendRules({
            subject = "swampdota_buyUnit",
            data = {
                unitName = unit
            },
        })
    end
end