local sensorInfo = {
	name = "UnitTypeTest",
	desc = "Returns type of the unit",
	author = "Jakub Stacho",
	date = "2021-07-01",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local armmartCount = global.group.armmart
local armboxCount = global.group.armbox
local armatlasCount = global.group.armatlas
local armmavCount = global.group.armmav
local armseerCount = global.group.armseer

function NumUnits(unit, supportUnits)

    if (supportUnits == nil) then
        return 0
    end
    
    local count = 0
    for _, v in pairs(supportUnits[unit]) do
        count = count + 1
    end

    return count
end

function BuyGroup(buyTable)

    for _, unit in ipairs(buyTable) do
        message.SendRules({
            subject = "swampdota_buyUnit",
            data = {
                unitName = unit
            },
        })
    end
end

-- @description return static position of the first unit
return function(supportUnits)

    if (supportUnits == nil) then
        return nil
    end
    
    -- clear dead units
    for k, units in pairs(supportUnits) do
        for unitID,_ in pairs(units) do
            if (not Spring.ValidUnitID(unitID)) then
                supportUnits[k][unitID] = nil
            end
        end
    end
    
    local sBoxCount = NumUnits("armbox", supportUnits)
    local sMartCount = NumUnits("armmart", supportUnits)
    local sAtlasCount = NumUnits("armatlas", supportUnits)
    local sMavCount = NumUnits("armmav", supportUnits)
    local sSeerCount = NumUnits("armseer", supportUnits)
    
    --Spring.Echo("ATLAS "..sAtlasCount.." "..armatlasCount)
    
    local enoughUnits = supportUnits ~= nil and (sMartCount >= armmartCount and sBoxCount >= armboxCount and sAtlasCount >= armatlasCount and sMavCount >= armmavCount and sSeerCount >= armseerCount)
    local teamMetal = Sensors.core.TeamMetal()
    teamMetal.currentLevel = teamMetal.currentLevel - global.price
    
    if (enoughUnits or teamMetal.currentLevel < 100) then
        return nil
    end
    
    local info = Sensors.core.MissionInfo()    
    local groupCost = 0
    local buyTable = {}
    local buyIndex = 1
       

    local quantity = (armatlasCount - sAtlasCount)
    local price = (info.buy["armatlas"] * quantity)
    
    if (quantity > 0 and sAtlasCount < armatlasCount and (groupCost + price < teamMetal.currentLevel)) then        
        groupCost = groupCost + price
        global.price = global.price + groupCost
        
        for i=1,quantity do
            buyTable[buyIndex] = "armatlas"
            buyIndex = buyIndex + 1
        end
    end
    
    quantity = armboxCount - sBoxCount
    price = (info.buy["armbox"] * quantity)
    
    if (quantity > 0 and sBoxCount < armboxCount and (groupCost + price < teamMetal.currentLevel)) then        
        groupCost = groupCost + price
        global.price = global.price + groupCost
        
        for i=1,quantity do
            buyTable[buyIndex] = "armbox"
            buyIndex = buyIndex + 1
        end
    end
    
    quantity = (armmartCount - sMartCount)
    price = (info.buy["armmart"] * quantity)
    
    if (quantity > 0 and sMartCount < armmartCount and (groupCost + price < teamMetal.currentLevel)) then       
        groupCost = groupCost + price
        global.price = global.price + groupCost
        
        for i=1,quantity do
            buyTable[buyIndex] = "armmart"
            buyIndex = buyIndex + 1
        end
        
    end
    
    quantity = (armseerCount - sSeerCount)
    price = (info.buy["armseer"] * quantity)
    
    if (quantity > 0 and sSeerCount < armseerCount and (groupCost + price < teamMetal.currentLevel)) then       
        groupCost = groupCost + price
        global.price = global.price + groupCost
        
        for i=1,quantity do
            buyTable[buyIndex] = "armseer"
            buyIndex = buyIndex + 1
        end
        
    end
    
    quantity = (armmavCount - sMavCount)
    price = (info.buy["armmav"] * quantity)     
    if (quantity > 0 and sMavCount < armmavCount and (groupCost + price < teamMetal.currentLevel)) then        
        groupCost = groupCost + price
        global.price = global.price + groupCost
        
        for i=1,quantity do
            buyTable[buyIndex] = "armmav"
            buyIndex = buyIndex + 1
        end
       
    end
    
    if (groupCost < teamMetal.currentLevel and groupCost > 0) then
        BuyGroup(buyTable)
        return buyTable
    end
    
    return nil   
end