local sensorInfo = {
	name = "DetectBattleLinePoint",
	desc = "Detects point of battle in each lane",
	author = "Jakub Stacho",
	date = "2021-07-01",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

function EnemiesInArea(point, radius)
    local enemyIDs = Sensors.core.EnemyTeamIDs()
    for _, enemyID in ipairs(enemyIDs) do    
        if (Spring.GetUnitsInCylinder(point.x, point.z, radius, enemyID)[1] ~= nil) then
            return true
        end
    end
    
    return false
end

function DetectLinePoint(missionInfo, lineName)

    local enemyIDs = Sensors.core.EnemyTeamIDs()
    local points = missionInfo.corridors[lineName].points
    local lastStrongPointOwn = nil
    local lastStrongPointEnemy = nil
    local boxPoint = nil
    local safePoint = nil
    for i=2, #points - 1 do        
        
        if (points[i].isStrongpoint and points[i].ownerAllyID == 0) then
            lastStrongPointOwn = points[i].position
        end
        
        if (points[i].isStrongpoint and points[i].ownerAllyID == 1) then
            lastStrongPointEnemy = points[i].position
            break
        end
    end
    
    if (lastStrongPointEnemy ~= nil) then
        local dir = lastStrongPointOwn - lastStrongPointEnemy
        safePoint = lastStrongPointOwn + (dir * 0.1)
        
        dir = (lastStrongPointEnemy - lastStrongPointOwn) * 0.7
        boxPoint = lastStrongPointOwn + dir + Vec3(-dir.z, dir.y, dir.x) * 0.9
    else    
        safePoint = lastStrongPointOwn
        boxPoint = lastStrongPointOwn
    end
    
    if ((boxPoint - Vec3(4600, 46, 5400)):Length() > 1500) then
        if (safePoint ~= nil) then
            boxPoint = safePoint
        else
            boxPoint = safePoint
        end
    end
    
    return { own = lastStrongPointOwn, enemy = lastStrongPointEnemy, safe = safePoint, box = boxPoint}    
end

-- @description return static position of the first unit
return function(missionInfo, battlePoints)	

    if (battlePoints == nil) then
        battlePoints = {}
    end
    
    battlePoints["Middle"] = DetectLinePoint(missionInfo, "Middle") 
    battlePoints["Top"] = DetectLinePoint(missionInfo, "Top")
    battlePoints["Bottom"] = DetectLinePoint(missionInfo, "Bottom")
    
    return battlePoints
end