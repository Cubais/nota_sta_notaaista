local sensorInfo = {
	name = "FreeUnits",
	desc = "Frees up unused transporters and not rescued units marked as occupied",
	author = "Jakub Stacho",
	date = "2021-07-09",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description frees up unused transporters and not rescued units marked as occupied
return function(reservationSystem)	    

	if (reservationSystem == nil) then
        return nil
    end
    
    for unitType, units in pairs(reservationSystem) do
        for unitID, state in pairs(units) do
            if ((Spring.ValidUnitID(unitID) and state ~= "free")) then
                reservationSystem[unitType][unitID] = "free"
            end
        end        
    end
    
	return reservationSystem
end