local sensorInfo = {
	name = "FreeUnits",
	desc = "Frees up unused transporters and not rescued units marked as occupied",
	author = "Jakub Stacho",
	date = "2021-07-09",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description frees up unused transporters and not rescued units marked as occupied
return function(reservationSystem)	    

	if (reservationSystem == nil) then
        return nil
    end
    
    for id, state in pairs(reservationSystem["armatlas"]) do
        
        if (Spring.ValidUnitID(id) and state == "occupied" and Spring.GetUnitCommands(id, 5)[1] == nil) then			
            reservationSystem["armatlas"][id] = "free"			
        end
    end
	return reservationSystem
end