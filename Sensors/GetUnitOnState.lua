local sensorInfo = {
	name = "GetFreeTransporter",
	desc = "Returns free transporter's unitID",
	author = "Jakub Stacho",
	date = "2021-07-09",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return static position of the first unit
return function(unitType, tableOfUnits, state)	
    local selectedID = nil

    if (tableOfUnits == nil) then
        return selectedID
    end
    
	for unitID, stateUnit in pairs(tableOfUnits[unitType]) do
		if (stateUnit == state) then            
			selectedID = unitID
			--tableOfUnits[unitType][unitID] = "occupied"
			break
		end
	end
	
	return selectedID
end