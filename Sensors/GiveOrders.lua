local sensorInfo = {
	name = "UnitTypeTest",
	desc = "Returns type of the unit",
	author = "Jakub Stacho",
	date = "2021-07-01",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function GetFreeUnit(unitName, unitsTable)

    for unitID, state in pairs(unitsTable[unitName]) do
        if (state == "free") then            
            return unitID
        end
    end
    return nil
end

-- @description return static position of the first unit
return function(supportUnits, pos)
        
    local freeAtlas = GetFreeUnit("armatlas", supportUnits)
    local freeBox = GetFreeUnit("armbox", supportUnits)    
    
    if (freeAtlas == nil or freeBox == nil) then
        return supportUnits
    end
    
    Spring.Echo(pos.x)
    SpringGiveOrderToUnit(freeAtlas, CMD.LOAD_UNITS, {freeBox}, {"shift"})
    SpringGiveOrderToUnit(freeAtlas, CMD.UNLOAD_UNITS, {pos.x, pos.y, pos.z, 300}, {"shift"})
    
    supportUnits["armatlas"][freeAtlas] = "taken"
    supportUnits["armbox"][freeBox] = "taken"
    
    
    return supportUnits
end