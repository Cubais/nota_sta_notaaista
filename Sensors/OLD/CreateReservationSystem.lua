local sensorInfo = {
	name = "UnitTypeTest",
	desc = "Creates reservation system",
	author = "Jakub Stacho",
	date = "2021-07-01",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return static position of the first unit
return function()	
    local allUnits = Spring.GetAllUnits()
	local atlases = Sensors.core.FilterUnitsByCategory(allUnits, Categories.nota_sta_notaaist.atlases)	
	local unitsToRescue = Spring.GetUnitsInRectangle(0, 600, Game.mapSizeX, Game.mapSizeZ, Spring.GetMyTeamID())
	local additionalUnit = Spring.GetUnitsInRectangle(7900, 0, Game.mapSizeX, 400, Spring.GetMyTeamID())
        
	local reservationSystem = {
		atlases = {},
		unitsToRescue = {}
	}
	
	for i=1, #atlases do 
		reservationSystem.atlases[atlases[i]] = "free"
	end
	
	for i=1, #unitsToRescue do 
		reservationSystem.unitsToRescue[unitsToRescue[i]] = "free"
	end
    
    reservationSystem.unitsToRescue[additionalUnit[1]] = "free"
		
	return reservationSystem
end