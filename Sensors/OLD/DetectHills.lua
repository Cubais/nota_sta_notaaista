local sensorInfo = {
	name = "DetectHills",
	desc = "Detects hils",
	author = "Jakub Stacho",
	date = "2021-07-01",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function newHillDetected(newPos, detectedHills)

    for _, hillPos in ipairs(detectedHills) do
        if ((hillPos - newPos):Length() < 512) then
            return false
        end
    end
    
    return true    
end

function findHills(referenceHeight)
    local hills = {}
    local mapX = Game.mapSizeX
    local mapZ = Game.mapSizeZ
    local key = units[1]
    for x = 0, mapX, 100 do
        for z = 0, mapZ, 100 do
            local height = Spring.GetGroundHeight(x, z)
            local pos = Vec3(x, height, z)
            if ((height > referenceHeight) and (newHillDetected(pos, hills)))  then                          
                table.insert(hills, pos)
            end            
        end
    end
    
    return hills
end

-- @description draw arrow indicating wind
return function(referenceHeight)

    local hills = findHills(referenceHeight)
    return {hills = hills}
	
end