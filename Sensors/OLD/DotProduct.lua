local sensorInfo = {
	name = "Vector dot prodcut",
	desc = "Returns dot product of vector.",
	author = "Cubais",
	date = "2021-06-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function(x, z)
    
    -- dot product against vector (0.5, 0, 0.5)
    local dot = (x * 0.5) + (z * 0.5)
	return {
		dot = dot
	}
end