local sensorInfo = {
	name = "EnemiesGridCosts",
	desc = "Fills table with enemy strength for each cell",
	author = "Cubais",
	date = "2021-07-07",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function Split(s, delimiter)
    result = {};
    for match in (s..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(result, match);
    end
    return result;
end

function CellPointsUnderThreshold(positionStr, minPoints, treshold) 
	local splitted = Split(positionStr, ",")
	local position = Vec3(tonumber(splitted[1]), 0, tonumber(splitted[2]))
	
	local rectSizeLength = Game.mapSizeX / 64
	
	
	local cellPoints = {}
	-- center Point
	cellPoints[1] = Vec3((position.x + 0.5) * rectSizeLength, y, (position.z + 0.5) * rectSizeLength)
	cellPoints[1].y = Spring.GetGroundHeight(cellPoints[1].x, cellPoints[1].z)	
	
	-- UpperLeft
	cellPoints[2] = Vec3((position.x) * rectSizeLength, y, (position.z) * rectSizeLength)
	cellPoints[2].y = Spring.GetGroundHeight(cellPoints[2].x, cellPoints[2].z)	
	
	-- LowerRight
	cellPoints[3] = Vec3((position.x + 1) * rectSizeLength, y, (position.z + 1) * rectSizeLength)
	cellPoints[3].y = Spring.GetGroundHeight(cellPoints[3].x, cellPoints[3].z)	
	
	-- LowerLeft
	cellPoints[4] = Vec3((position.x) * rectSizeLength, y, (position.z + 1) * rectSizeLength)
	cellPoints[4].y = Spring.GetGroundHeight(cellPoints[4].x, cellPoints[4].z)	
	
	-- UpperRight
	cellPoints[5] = Vec3((position.x + 1) * rectSizeLength, y, (position.z) * rectSizeLength)
	cellPoints[5].y = Spring.GetGroundHeight(cellPoints[5].x, cellPoints[5].z)
	
	local pointsUnderTreshold = 0
	for _, v in ipairs(cellPoints) do
		if (v.y <= treshold) then
			pointsUnderTreshold = pointsUnderTreshold + 1			
		end
	end
	
	return (pointsUnderTreshold >= minPoints)
end

function UpdateCellWithNeighbours(costTable, cellX, cellZ, enemyX, enemyZ)
    local tableKey = tostring(cellX .. "," .. cellZ)

    -- update cell because enemy is inside
    if (costTable[tableKey] == nil) then
        costTable[tableKey] = 300
    else
        costTable[tableKey] = costTable[tableKey] + 300
    end
    
    -- update cells around to avoid them as well
    local count = 1
    local neighbourKeys = {}    
	
	for x = -5,5,1 do
		for z = -5,5,1 do
		
			local nCellX = cellX + x
			local nCellZ = cellZ + z
			if (nCellX >= 0 and nCellX <= 64 and nCellZ >= 0 and nCellZ <= 64 and (x ~= 0 and z ~= 0))  then 
				neighbourKeys[count] = tostring(nCellX .. "," .. nCellZ)
				count = count + 1
			end				
		end	
	end
    
    -- updateNeighbour
    for _, nKey in ipairs(neighbourKeys) do
        if (costTable[nKey] == nil or costTable[nKey] == 0) then
		
			local splitted = Split(nKey, ",")
			local position = Vec3(tonumber(splitted[1]), 0, tonumber(splitted[2]))
			
			if (not CellPointsUnderThreshold(nKey, 4, 200)) then
				costTable[nKey] = 100
			end
			
			if (CellPointsUnderThreshold(nKey, 4, 150)) then
				costTable[nKey] = 0
			end			
        end
    end
    
    return costTable
end

-- @description returns grid with values representing enemy strength in the cell
return function(gridCostTable)
	
    if (gridCostTable == nil) then 
        gridCostTable = {}
    end
    
    local tempCostTable = {}
    local unitList = Sensors.core.EnemyUnits()
    
    for index, unit in ipairs(unitList) do
        local unitID = unit
        local x,y,z = Spring.GetUnitPosition(unitID)
        
        local rectSizeLength = Game.mapSizeX / 64	
        
        -- determine x and z position of square we are into
        local rectX = math.floor(x / rectSizeLength)
        local rectZ = math.floor(z / rectSizeLength)
        
        tempCostTable = UpdateCellWithNeighbours(tempCostTable, rectX, rectZ, x, z)
    end
    
    -- update gridCostTable with newer values
    for key, _ in pairs(tempCostTable) do
        if (gridCostTable[key] == nil or tempCostTable[key] > gridCostTable[key]) then
            gridCostTable[key] = tempCostTable[key]
        end    
    end
    
	return gridCostTable
end