local sensorInfo = {
	name = "FindSafePath",
	desc = "Finds safe path to the given destination",
	author = "Jakub Stacho",
	date = "2021-07-01",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function GetTableSize(table)
	local size = 0
	for _ in pairs(table) do size = size + 1 end

	return size
end


function GetLowestFScore(fScoreTable, fromList)
	local node, lowest = nil, 1000000	
	for k, v in pairs(fromList) do
		if (fScoreTable[k] < lowest) then
			lowest = fScoreTable[k]
			node = k
		end
	end
	
	return node
end

function GetManhatanDistance(from, goal)
    local splitted1 = Split(from, ",")
    local splitted2 = Split(goal, ",")
    local x , y = tonumber(splitted1[1]), tonumber(splitted1[2])
    local goalX , goalY = tonumber(splitted2[1]), tonumber(splitted2[2])
    
	return (math.abs(x - goalX) + math.abs(y - goalY))
end

--[[function GetManhatanDistance(x, y, goalX, goalY)
	return (math.abs(x - goalX) + math.abs(y - goalY))
end]]--

function Split(s, delimiter)
    result = {};
    for match in (s..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(result, match);
    end
    return result;
end

function GetNeighboursKeys(node)
	local values = Split(node, ",")
	local x, z = tonumber(values[1]), tonumber(values[2])
	local neighbours = {}
	if (x ~= 0) then
		neighbours[tostring((x - 1) .. "," .. z)] = 0
	end
	
	if (z ~= 0) then
		neighbours[tostring(x .. "," .. (z - 1))] = 0
	end
	
	if (x ~= 64) then
		neighbours[tostring((x + 1) .. "," .. z)] = 0
	end
	
	if (z ~= 64) then
		neighbours[tostring(x .. "," .. (z + 1))] = 0
	end
	
	return neighbours
end

function DeterminePath(pathData, startKey, goalKey)
	local currentKey = goalKey
	local path = {}

	while (currentKey ~= startKey) do
		table.insert( path, 1, currentKey)
		currentKey = pathData[currentKey]	
	end

	table.insert( path, 1, startKey)
	return path
end

function FindPath(startX, startZ, endX, endZ, cellCosts)
	local openList = {}
	local closedList = {}
	local goalKey = tostring(endX .. "," .. endZ)
	local startKey = tostring(startX .. "," .. startZ)
	local finished = false
		
	math.randomseed(os.time()) -- random initialize
	math.random(); math.random(); math.random() -- warming up

	-- used to reconstruct path after algorithm
	local pathFrom = {}
	
	openList[tostring(startX .. "," .. startZ)] = 0	
	
	-- init Fs scores
	local fScore = {}
	fScore[tostring(startX .. "," .. startZ)] = 0
	
	while (GetTableSize(openList) > 0) do
		local currentNode = GetLowestFScore(fScore, openList)
		
		local neighbours = GetNeighboursKeys(currentNode)
		for neighbour, _ in pairs(neighbours) do
			
            -- reached destination
			if (neighbour == goalKey) then 
				fScore[goalKey] = fScore[currentNode]
				pathFrom[goalKey] = currentNode
				finished = true
				break
			end
			
            local neighbourEnemies = 0
                if (cellCosts[neighbour] ~= nil) then
                    neighbourEnemies = cellCosts[neighbour]
                end
            
			-- new node found
			if (closedList[neighbour] == nil and openList[neighbour] == nil and neighbourEnemies == 0) then 				
				-- add to openList
				openList[neighbour] = 0
                
				-- update fScore
				fScore[neighbour] = fScore[currentNode] + 2 * neighbourEnemies  + GetManhatanDistance(neighbour, goalKey)
				pathFrom[neighbour] = currentNode		
				
			--[[elseif (closedList[neighbour] == nil and fScore[neighbour] > (fScore[currentNode] + 2 * neighbourEnemies + GetManhatanDistance(neighbour, goalKey))) then                
                fScore[neighbour] = fScore[currentNode] + 2 * neighbourEnemies + GetManhatanDistance(neighbour, goalKey)
                pathFrom[neighbour] = currentNode]]--
                
            end
		end
		
        -- remove from open list and add to closed list
		openList[currentNode] = nil
		closedList[currentNode] = 0
		
		if (finished) then
			break
		end
	end
	
	return DeterminePath(pathFrom, startKey, goalKey)
end

function GetLowestWorldPositionOnCell(positionStr)
	local splitted = Split(positionStr, ",")
	local position = Vec3(tonumber(splitted[1]), 0, tonumber(splitted[2]))
	
	local rectSizeLength = Game.mapSizeX / 64
	
	
	local cellPoints = {}
	-- center Point
	cellPoints[1] = Vec3((position.x + 0.5) * rectSizeLength, y, (position.z + 0.5) * rectSizeLength)
	cellPoints[1].y = Spring.GetGroundHeight(cellPoints[1].x, cellPoints[1].z)	
	
	-- UpperLeft
	cellPoints[2] = Vec3((position.x) * rectSizeLength, y, (position.z) * rectSizeLength)
	cellPoints[2].y = Spring.GetGroundHeight(cellPoints[2].x, cellPoints[2].z)	
	
	-- LowerRight
	cellPoints[3] = Vec3((position.x + 1) * rectSizeLength, y, (position.z + 1) * rectSizeLength)
	cellPoints[3].y = Spring.GetGroundHeight(cellPoints[3].x, cellPoints[3].z)	
	
	-- LowerLeft
	cellPoints[4] = Vec3((position.x) * rectSizeLength, y, (position.z + 1) * rectSizeLength)
	cellPoints[4].y = Spring.GetGroundHeight(cellPoints[4].x, cellPoints[4].z)	
	
	-- UpperRight
	cellPoints[5] = Vec3((position.x + 1) * rectSizeLength, y, (position.z) * rectSizeLength)
	cellPoints[5].y = Spring.GetGroundHeight(cellPoints[5].x, cellPoints[5].z)	
	
	
	local result = cellPoints[1]
	local lowest = cellPoints[1].y
		
	-- returns lowest point of the cell
	for _, point in ipairs(cellPoints) do
		if (point.y < lowest) then
			result = point
			lowest = point.y
		end
	end
	
	return result
end


-- @description returns table of positions to along safe path
-- computed using A* to go through positions with least enemy presence
return function(unitID, destination, tableWithCosts)
		
		local rectSizeLength = Game.mapSizeX / 64	
	
		local x, y, z = Spring.GetUnitPosition(unitID)		
		
		local posX = math.floor(x / rectSizeLength)
		local posZ = math.floor(z / rectSizeLength)
		local goalX = math.floor(destination.x / rectSizeLength)
		local goalZ = math.floor(destination.z / rectSizeLength)
        
        local pathPositions = {}
        local count = 1
						
		local path = FindPath(posX, posZ, goalX, goalZ, tableWithCosts)		
        for k, v in pairs(path) do
            local position = GetLowestWorldPositionOnCell(v)
            
			local prevPosition = pathPositions[count - 1]
			if (prevPosition == nil or (prevPosition.x ~= position.x and prevPosition.z ~= position.z)) then
				if (prevPosition == nil) then
					pathPositions[count] = position
				else
					position = GetLowestWorldPositionOnCell(path[k - 1])
					
					pathPositions[count] = position
				end
				count = count + 1  
			end
        end
		
		--pathPositions[count] = GetLowestWorldPositionOnCell(path[#path])
		return pathPositions
end