local sensorInfo = {
	name = "FreeUnits",
	desc = "Frees up unused transporters and not rescued units marked as occupied",
	author = "Jakub Stacho",
	date = "2021-07-09",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description frees up unused transporters and not rescued units marked as occupied
return function(reservationSystem, transporterID, rescueUnitID, safeArea)	    
	
	-- transporter is marked as occupied but has nothing to do
	if (Spring.ValidUnitID(transporterID) and reservationSystem.atlases[transporterID] == "occupied" and Spring.GetUnitCommands(transporterID, 5)[1] == nil) then			
		reservationSystem.atlases[transporterID] = "free"			
	end
	
	-- if transporter dead, remove from reservation system
	if (not Spring.ValidUnitID(transporterID)) then
		reservationSystem.atlases[transporterID] = nil			
	end
	-- Check if units are in safe Area
	Sensors.SafeRescueUnits(reservationSystem, safeArea)
	
	-- transporter dead but rescueUnit is not, free it
	if ((not Spring.ValidUnitID(transporterID)) and Spring.ValidUnitID(rescueUnitID) and reservationSystem.unitsToRescue[rescueUnitID] == "occupied") then			
		reservationSystem.unitsToRescue[rescueUnitID] = "free"			
	end
	
	return true
end