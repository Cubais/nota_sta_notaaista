local sensorInfo = {
	name = "UnitTypeTest",
	desc = "Returns position of the given unit",
	author = "Jakub Stacho",
	date = "2021-07-01",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return static position of the first unit
return function(unitID)	
    local x, y, z = Spring.GetUnitPosition(unitID)
	return Vec3(x, y, z)
end