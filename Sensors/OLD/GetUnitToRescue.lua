local sensorInfo = {
	name = "GetUnitToRescue",
	desc = "Returns free unit to be rescued ID",
	author = "Jakub Stacho",
	date = "2021-07-09",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return static position of the first unit
return function(reservationSystem)	
    local selectedID
	for unitID, state in pairs(reservationSystem.unitsToRescue) do
		if (state == "free") then
			selectedID = unitID
			reservationSystem.unitsToRescue[unitID] = "occupied"
			break
		end
	end
	
	return selectedID
end