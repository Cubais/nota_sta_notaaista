local sensorInfo = {
	name = "HighlightDangerCells",
	desc = "Highlights cells where enemy are present",
	author = "Cubais",
	date = "2021-07-07",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function Split(s, delimiter)
    result = {};
    for match in (s..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(result, match);
    end
    return result;
end



-- @description returns unit position and X and Z coordinate of rectangle
return function(tableWithCosts)
	
    if (tableWithCosts == nil) then return  false end
    local index = 0    
    for key, value in pairs(tableWithCosts) do
    
		if (value > 0) then
			local splitted = Split(key, ",")
			local rectX, rectZ = tonumber(splitted[1]), tonumber(splitted[2])        
			
			local rectSizeLength = Game.mapSizeX / 64	
			
			local botLeft = Vec3(rectX * rectSizeLength, y, (rectZ + 1) * rectSizeLength)
			botLeft.y = Spring.GetGroundHeight(botLeft.x, botLeft.z)
			
			local botRight = Vec3((rectX + 1) * rectSizeLength, y, (rectZ + 1) * rectSizeLength)
			botRight.y = Spring.GetGroundHeight(botRight.x, botRight.z)
			
			local topLeft = Vec3(rectX * rectSizeLength, y, rectZ * rectSizeLength)
			topLeft.y = Spring.GetGroundHeight(topLeft.x, topLeft.z)
			
			local topRight = Vec3((rectX + 1) * rectSizeLength, y, rectZ * rectSizeLength)
			topRight.y = Spring.GetGroundHeight(topRight.x, topRight.z)
			
			if (Script.LuaUI('exampleDebug_update')) then
				Script.LuaUI.exampleDebug_update(
					(index * 4) + 1, -- key
					{	-- data
						startPos = botLeft, 
						endPos = botRight
					}
				)
				Script.LuaUI.exampleDebug_update(
					(index * 4) + 2, -- key
					{	-- data
						startPos = botLeft, 
						endPos = topLeft
					}
				)
				Script.LuaUI.exampleDebug_update(
					(index * 4) + 3, -- key
					{	-- data
						startPos = topLeft, 
						endPos = topRight
					}
				)
				Script.LuaUI.exampleDebug_update(
					(index * 4) + 4, -- key
					{	-- data
						startPos = topRight, 
						endPos = botRight
					}
				)
			end
			
			index = index + 1
		end
    end
	return {	index
			}
end