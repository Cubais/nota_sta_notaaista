local sensorInfo = {
	name = "InRectangleDebug",
	desc = "Highlights rectangle on map based on unit position",
	author = "Cubais",
	date = "2021-07-03",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description returns unit position and X and Z coordinate of rectangle
return function()
	
	local unitID = units[1]
	local x,y,z = Spring.GetUnitPosition(unitID)
	
	local rectSizeLength = Game.mapSizeX / 64	
	
	-- determine x and z position of square we are into
	local rectX = math.floor(x / rectSizeLength)
	local rectZ = math.floor(z / rectSizeLength)
	
	local botLeft = Vec3(rectX * rectSizeLength, y, (rectZ + 1) * rectSizeLength)
	botLeft.y = Spring.GetGroundHeight(botLeft.x, botLeft.z)
	
	local botRight = Vec3((rectX + 1) * rectSizeLength, y, (rectZ + 1) * rectSizeLength)
	botRight.y = Spring.GetGroundHeight(botRight.x, botRight.z)
	
	local topLeft = Vec3(rectX * rectSizeLength, y, rectZ * rectSizeLength)
	topLeft.y = Spring.GetGroundHeight(topLeft.x, topLeft.z)
	
	local topRight = Vec3((rectX + 1) * rectSizeLength, y, rectZ * rectSizeLength)
	topRight.y = Spring.GetGroundHeight(topRight.x, topRight.z)
	
	if (Script.LuaUI('exampleDebug_update')) then
		Script.LuaUI.exampleDebug_update(
			1, -- key
			{	-- data
				startPos = botLeft, 
				endPos = botRight
			}
		)
		Script.LuaUI.exampleDebug_update(
			2, -- key
			{	-- data
				startPos = botLeft, 
				endPos = topLeft
			}
		)
		Script.LuaUI.exampleDebug_update(
			3, -- key
			{	-- data
				startPos = topLeft, 
				endPos = topRight
			}
		)
		Script.LuaUI.exampleDebug_update(
			4, -- key
			{	-- data
				startPos = topRight, 
				endPos = botRight
			}
		)
	end
		
	return {	Sensors.MaxWeaponRange(unitID)
			}
end