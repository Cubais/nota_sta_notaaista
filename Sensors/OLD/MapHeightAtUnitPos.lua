local sensorInfo = {
	name = "MapHeightAtUnitPos",
	desc = "Returns height of the terrain at unit position",
	author = "Jakub Stacho",
	date = "2021-07-01",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description return static position of the first unit
return function()
	local x,y,z = SpringGetUnitPosition(units[1])
	return Vec3(x,y,z)
end