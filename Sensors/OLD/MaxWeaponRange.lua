local sensorInfo = {
	name = "Vector dot prodcut",
	desc = "Returns dot product of vector.",
	author = "Cubais",
	date = "2021-06-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function(unitID)
    
	local defID = Spring.GetUnitDefID(unitID)
	local weapons = UnitDefs[defID].weapons
	
	local maxRange = 0
	
	if (weapons == nil) then return 0 end
	for k, v in ipairs(weapons) do 
		local weaponDef = v.weaponDef
		if (WeaponDefs[weaponDef].range > maxRange) then
			maxRange = WeaponDefs[weaponDef].range
		end
	end
	
	return maxRange
end