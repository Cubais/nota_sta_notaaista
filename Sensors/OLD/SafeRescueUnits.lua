local sensorInfo = {
	name = "FreeRescueUnits",
	desc = "Frees up units to be rescued",
	author = "Jakub Stacho",
	date = "2021-07-09",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

function IsInSafeArea(rescueUnitID, safeArea)

	local unitsInArea = Spring.GetUnitsInSphere(safeArea.center.x, safeArea.center.y, safeArea.center.z, safeArea.radius)
	for _, unitID in pairs(unitsInArea) do
		if (unitID == rescueUnitID) then
			return true
		end
	end
	
	return false
end

-- @description frees up unused transporters
return function(reservationSystem, safeArea)	    
	for unitID, state in pairs(reservationSystem.unitsToRescue) do
		if (Spring.ValidUnitID(unitID) and state == "occupied") then			
			if (IsInSafeArea(unitID, safeArea)) then
				reservationSystem.unitsToRescue[unitID] = "safe"
			end
		end
	end
	
	return true
end