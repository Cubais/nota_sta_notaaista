local sensorInfo = {
	name = "TransportersMoving",
	desc = "Returns true if some transporter is executing command, false if no transporter is exec. command",
	author = "Jakub Stacho",
	date = "2021-07-09",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return static position of the first unit
return function(reservationSystem)    
	for unitID, state in pairs(reservationSystem.atlases) do
		if (Spring.GetUnitCommands(unitID, 5)[1] ~= nil) then
			return true
		end
	end
	
	return false
end