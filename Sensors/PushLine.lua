local sensorInfo = {
	name = "UnitTypeTest",
	desc = "Returns type of the unit",
	author = "Jakub Stacho",
	date = "2021-07-01",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

function NumEntries(lineTable)
    local count = 0
    for _,_ in pairs(lineTable) do
        count = count + 1
    end
    
    return count
end

-- @description return static position of the first unit
return function(mavericsTable, category)
    
    if (mavericsTable == nil) then
        mavericsTable = {}
        return mavericsTable
    end
        
    local line = nil
    if (global.battlePoints["Middle"].enemy ~= nil) then
        line = "Middle"
    elseif (global.battlePoints["Top"].enemy ~= nil) then
        line = "Top"
    elseif (global.battlePoints["Bottom"].enemy ~= nil) then
        line = "Bottom"
    end        
    
    local count = 0
    for id,_ in pairs(mavericsTable) do
        if (not Spring.ValidUnitID(id)) then
            mavericsTable[id] = nil
        else
            count = count + 1
        end
    end
    
    local units = Spring.GetUnitsInCylinder( 1400, 8900, 1000)
    local maverics = Sensors.core.FilterUnitsByCategory(units, category)
    
    if (#maverics < 10 and count < 17) then    
        return false
    end
    
    local half = count / 2
    local dir = (global.battlePoints[line].own - global.battlePoints[line].enemy) / 10
    local targetPos = global.battlePoints[line].enemy
    local perpLeft = (Vec3(-dir.z, dir.y, dir.x))
    local perpRight = targetPos + (Vec3(dir.z, dir.y, -dir.x) * (half / 2))
    
    count = 1
    for id,_ in pairs(mavericsTable) do        
        local pos = perpRight + perpLeft * count
        if (count > half) then 
            pos = perpRight + perpLeft * (count - half) + (dir)
        end
        Spring.GiveOrderToUnit(id, CMD.MOVE, {pos.x, pos.y, pos.z}, {})
        count = count + 1
    end 
    
    return true
end