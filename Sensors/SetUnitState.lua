local sensorInfo = {
	name = "FreeUnits",
	desc = "Frees up unused transporters and not rescued units marked as occupied",
	author = "Jakub Stacho",
	date = "2021-07-09",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(reservationSystem, unitID, state)	    
	
	-- frees unit
	if (Spring.ValidUnitID(unitID)) then
        local unitName = UnitDefs[Spring.GetUnitDefID(unitID)].name
		reservationSystem[unitName][unitID] = state			
        
        if (unitName == "armatlas" and state == "free") then
            local info = Sensors.core.MissionInfo()
            local homePoint = info.corridors["Top"].points[1].position
            Spring.GiveOrderToUnit(unitID, CMD.MOVE, {homePoint.x, homePoint.y, homePoint.z}, {}) 
            reservationSystem["armatlas"][unitID] = "free"
        end
	end
    
    
	
	return reservationSystem
end