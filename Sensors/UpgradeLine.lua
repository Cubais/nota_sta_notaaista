local sensorInfo = {
	name = "UnitTypeTest",
	desc = "Returns type of the unit",
	author = "Jakub Stacho",
	date = "2021-07-01",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return static position of the first unit
return function(missionInfo, linesInfo)
    local teamMetal = Sensors.core.TeamMetal()    
    local lineCost = missionInfo.upgrade.line
    
    if (teamMetal.currentLevel < lineCost or teamMetal.pull >= lineCost) then
        return linesInfo
    end
    
    if (linesInfo == nil) then
        linesInfo = {}
        linesInfo["Top"] = 1
        linesInfo["Bottom"] = 1
        linesInfo["Middle"] = 1        
    end        
    
    if (linesInfo["Middle"] < missionInfo.upgrade.lineMaxLevel) then
        message.SendRules({
            subject = "swampdota_upgradeLine",
            data = {
                lineName = "GoodMiddle",
                upgradeLevel = 1,
            },
        })
        
        linesInfo["Middle"] = linesInfo["Middle"] + 1    
        
        return linesInfo
    end
    
    if (linesInfo["Top"] < missionInfo.upgrade.lineMaxLevel) then
        message.SendRules({
            subject = "swampdota_upgradeLine",
            data = {
                lineName = "GoodTop",
                upgradeLevel = 1,
            },
        })
        
        linesInfo["Top"] = linesInfo["Top"] + 1
        
        return linesInfo
    end
    
    if (linesInfo["Bottom"] < missionInfo.upgrade.lineMaxLevel) then
        message.SendRules({
            subject = "swampdota_upgradeLine",
            data = {
                lineName = "GoodBottom",
                upgradeLevel = 1,
            },
        })
        
        linesInfo["Bottom"] = linesInfo["Bottom"] + 1
        
        return linesInfo
    end
    
    return linesInfo
end